import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'register', pathMatch: 'full' },
  { path: 'register', loadChildren: './auth/register/register.module#RegisterPageModule' },
  { path: 'login', loadChildren: './auth/login/login.module#LoginPageModule' },
  { path: 'dashboard', loadChildren: './home/dashboard/dashboard.module#DashboardPageModule' },
  { path: 'profile', loadChildren: './onboarding/profile/profile.module#ProfilePageModule' },
  { path: 'confirmation', loadChildren: './auth/confirmation/confirmation.module#ConfirmationPageModule' },
  { path: 'confirm', loadChildren: './modal/confirm/confirm.module#ConfirmPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
