import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ConfirmationPage } from './confirmation.page';
import { ConfirmPage } from 'src/app/modal/confirm/confirm.page';

const routes: Routes = [
  {
    path: '',
    component: ConfirmationPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ConfirmationPage], // , ConfirmPage
  // entryComponents:[ConfirmPage]
})
export class ConfirmationPageModule {}
