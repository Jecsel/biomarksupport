import { Component, OnInit } from '@angular/core';
import { ModalController, AlertController } from '@ionic/angular';
import { ConfirmPage } from 'src/app/modal/confirm/confirm.page';
import { AuthProvider } from 'src/app/providers/authProvider';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';

@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.page.html',
  styleUrls: ['./confirmation.page.scss'],
})
export class ConfirmationPage implements OnInit {
  user: string;
  pass: string;
  otp: string;
  constructor(public modalController: ModalController, private authProvider: AuthProvider, 
    private storage: Storage, public alertController: AlertController, private route: Router) { }

    async presentAlert(message) {
      const alert = await this.alertController.create({
        header: 'Alert',
        message: message,
      });
      await alert.present();
    }

  ngOnInit() {
    this.storage.get('username').then((val) => {
      this.user = val;
    });
    this.storage.get('password').then((val) => {
      this.pass = val;
    });
  }

  async verify() {
    if (this.otp == '' || this.otp == undefined || this.otp.length < 6) {
      this.presentAlert('Invalid OTP Code');
    } else {
      this.authProvider
        .confirm(this.user, this.pass, this.otp)
        .subscribe((response) => {
          this.route.navigate(['/confirm']);
        },
        err => {
          this.presentAlert(err.error.message);
        }
        );

      // const modal = await this.modalController.create({
      //   component: ConfirmPage
      // });
      // modal.present();
    }
  }

}
