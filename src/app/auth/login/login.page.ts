import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { AuthProvider } from 'src/app/providers/authProvider';
import { ToastController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  mobile: string;
  password: string;
  constructor(private route: Router, private authProvider: AuthProvider, public alertController: AlertController) { }

  async presentAlert(message) {
    const alert = await this.alertController.create({
      header: 'Alert',
      message: message,
    });
    await alert.present();
  }

  goRegister() {
    this.route.navigate(['/register']);
  }

  ngOnInit() {
  }

  telInputObject(obj) {
    obj.setCountry('ph');
  }

  hasError(obj) {
  }

  getNumber(obj) {
    this.mobile = obj
  }

  onCountryChange(obj) {
  }

  goLogin() {
    if (this.mobile == undefined || this.mobile == '' || this.mobile == null) {
      this.presentAlert('Mobile number fields is empty');
    } else {
      if (this.password == undefined || this.password.length < 8) {
        this.presentAlert('Password must be 8 digits');
      } else {
        this.authProvider
          .login(this.mobile, this.password)
          .subscribe((response) => {
            let navigationExtras: NavigationExtras = {
              queryParams: {
                token: response.access_token,
                mobile: this.mobile
              }
            };

            if (!response.has_profile) {
              this.route.navigate(['/profile'], navigationExtras);
            } else {
              localStorage.setItem("x-biomark-token", response.access_token);
              this.route.navigate(['/dashboard']);
            }
          },
            err => {
              this.presentAlert(err.error.message);
            });
      }
    }
  }
}
