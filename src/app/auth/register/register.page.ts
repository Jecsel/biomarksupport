import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthProvider } from 'src/app/providers/authProvider';
import { Storage } from '@ionic/storage';
import { ToastController, AlertController } from '@ionic/angular';
import { ToastOptions } from '@ionic/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  mobile: string;
  password: string;
  toastOptions: ToastOptions;

  constructor(private route: Router, private autoProvider: AuthProvider, private storage: Storage,
    public alertController: AlertController) {
  }

  async presentAlert(message) {
    const alert = await this.alertController.create({
      header: 'Alert',
      message: message,
    });
    await alert.present();
  }

  goLogin() {
    this.route.navigate(['/login']);
  }

  ngOnInit() {
  }

  telInputObject(obj) {
    obj.setCountry('ph');
  }

  hasError(obj) {
  }

  getNumber(obj) {
    this.mobile = obj;
  }

  onCountryChange(obj) {
  }

  goRegister() {
    if (this.mobile == undefined || this.mobile == '' || this.mobile == null) {
      this.presentAlert('Mobile number fields is empty');
    } else {
      if (this.password == undefined || this.password.length < 8) {
        this.presentAlert('Password must be 8 digits');
      } else {
        this.storage.set('username', this.mobile);
        this.storage.set('password', this.password);
        this.autoProvider
          .register(true, 'phlebotomist', this.mobile, this.password)
          .subscribe((response) => {
            this.route.navigate(['/confirmation']);
          },
            err => {
              this.presentAlert(err.error.message);
            });
      }
    }
  }

}
