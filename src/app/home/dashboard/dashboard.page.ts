import { Component, OnInit } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { DashProvider } from 'src/app/providers/dashProvider';
import { Router } from '@angular/router';
import { AuthProvider } from 'src/app/providers/authProvider';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {
  qrData = null;
  createCode = null;
  scanCode = null;
  dashData = {};
  barcodeData = {};

  controller = document.querySelector('ion-loading-controller');

  constructor(private barcodeScanner: BarcodeScanner, private dashProvider: DashProvider,
    private route: Router, private authProvider: AuthProvider) { }

  ngOnInit() {
    this.dashProvider
      .init()
      .subscribe((response) => {
        this.dashData = response;
      });
  }

  createdCode() {
    this.createCode = this.qrData;
  }

  showScanner() {
    this.scanCode = false;
    this.barcodeScanner.scan().then(barcodeData => {
      this.scan(barcodeData.text)
    })

    // this.barcodeScanner.scan().then(barcodeData => {
    //   console.log('Barcode data', barcodeData);
    // }).catch(err => {
    //   console.log('Error', err);
    // });
  }

  showInput() {
    this.scanCode = false;
    const alert = document.createElement('ion-alert');
    alert.header = 'Input code';
    alert.inputs = [
      {
        placeholder: 'Barcode'
      }
    ];
    alert.buttons = [
      {
        text: 'Scan',
        handler: (code) => {
          this.scan(code["0"])
        }
      }
    ];

    document.body.appendChild(alert);
    return alert.present();
  }

  scan(code){
    this.dashProvider
      .scan(code)
      .subscribe((response) => {
        if(response.status){
          this.scanCode = true;
          this.barcodeData = response;
        }else{
          this.showAlert();
        }
      });
  }

  showAlert(){
    const alert = document.createElement('ion-alert');
    alert.header = 'Scan error';
    alert.subHeader = "This barcode hasn't been scanned yet"
    alert.buttons = [
      {
        text: 'Dismiss',
      }
    ];

    document.body.appendChild(alert);
    return alert.present();
  }

  showLoading() {
    this.controller.create({
      message: 'Loading',
      // duration: 3000
    }).then(loading => loading.present());
  }

  goLogout(){
    console.log("logoout in dashboard");
    this.authProvider
      .logout()
      .subscribe((response) => {
        
    });
    localStorage.setItem("x-biomark-token", "");
    this.route.navigate(['/login']);
  }
}
