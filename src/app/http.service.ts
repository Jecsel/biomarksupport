import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// import { HTTPOriginal } from '@ionic-native/http';
import { BASE_URL } from 'src/environments/environment';
import { tokenName } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient) {} // HttpClient
  
  post(url, params){
    
    var headers = {}
    if(localStorage.getItem("x-biomark-token") !== null && localStorage.getItem("x-biomark-token") !== ""){
      var token = localStorage.getItem("x-biomark-token");
      headers = { headers: { 
        'x-biomark-group': 'phlebotomist',
        'x-biomark-token': token
      }};
    }else{
      headers = { headers: { 
        'x-biomark-group': 'phlebotomist'
      }};
    }

    return this.http.post(BASE_URL + url, params, headers)
  }

  get(url){
    var headers = {}
    if(localStorage.getItem("x-biomark-token") !== null && localStorage.getItem("x-biomark-token") !== ""){
      var token = localStorage.getItem("x-biomark-token");
      headers = { headers: { 
        'x-biomark-group': 'phlebotomist',
        'x-biomark-token': token
      }};
    }else{
      alert("Unauthorized");
      return
    }
    return this.http.get(BASE_URL + url, headers)
  }

  post_with_token(url, params, token){
    let headers = { headers: { 
      'x-biomark-group': 'phlebotomist',
      'x-biomark-token': token
    }};
    return this.http.post(BASE_URL + url, params, headers)
  }
}
