import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { modalController } from '@ionic/core';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.page.html',
  styleUrls: ['./confirm.page.scss'],
})
export class ConfirmPage implements OnInit {

  constructor(private route: Router, modalCtrl: ModalController) { }

  ngOnInit() {
  }

  backHome(){
    this.route.navigate(['login']);
    modalController.dismiss();
  }

}
