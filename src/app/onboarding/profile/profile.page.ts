import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { HttpService } from 'src/app/http.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  lastName: string;
  firstName: string;
  middleName: string;
  ic: string;
  birthday: Date;
  employer: string;
  gender_id: bigint;
  email: string;

  constructor(private route: ActivatedRoute, private router: Router, private httpService: HttpService) { }

  ngOnInit() {
  }

  goContinue() {
    this.route.queryParams.subscribe(params => {
      if (params) {
        console.log(params)
        var token = params.token;
        var mobile = params.mobile;
        let url = "phleb/profile"
        let requestBody = {
              profile: {
              last_name: this.lastName,
              first_name: this.firstName,
              middle_name: this.middleName,
              ic: this.ic,
              birthday: this.birthday,
              employer: this.employer,
              gender_id: this.gender_id,
              email: this.email,
              mobile: mobile
            }
        }
        this.httpService
          .post_with_token(url, requestBody, token)
          .subscribe((response) => {
            console.log(response)
            localStorage.setItem("x-biomark-token", token);
            this.router.navigate(['/dashboard']);
            return console.log(response)
          });
      }
    });
    // this.http.post('http://localhost:3000/profile', {profiles: {
    //   last_name: this.lastName,
    //   first_name: this.firstName,
    //   middle_name: this.middleName,
    //   ic: this.ic,
    //   birthday: this.birthday,
    //   phone_number: this.phoneNumber,
    //   nationality: this.nationality,
    //   employer: this.employer,
    //   gender: this.gender,
    //   email: this.email,
    //   }
    // }).subscribe((response) => {
    //   this.route.navigate(['/dashboard']);
    //   // return console.log(response);
    // });
  }

}
