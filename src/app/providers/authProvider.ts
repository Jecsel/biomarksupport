import { Injectable } from '@angular/core';
import { HttpService } from '../http.service';



@Injectable()
export class AuthProvider {

    login(mobile, password): any {
        var url = 'v2/sessions';
        var requestBody = {
            session: {
                username: mobile,
                password: password
            }
        }
        return this.httpService.post(url, requestBody)
    }
    register(terms, group, number, password){
        var url = "v2/users/signup"
        var requestBody = {
            user: {
                terms: terms,
                group: group,
                username: number,
                password: password
            }
        }
        return this.httpService.post(url, requestBody)
    }
    confirm(user, pass, otp): any {
        var url = '/v2/users/confirm';
        var requestBody = {
            user: {
                username: user,
                password: pass,
                code: otp
            }
        }
        return this.httpService.post(url, requestBody)
    }

    logout():any{
        var url = "v1/sessions/destroy ";
        return this.httpService.post(url, null);
    }


    constructor(private httpService: HttpService) { }


}