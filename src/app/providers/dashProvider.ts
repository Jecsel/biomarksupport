import { Injectable } from '@angular/core';
import { HttpService } from '../http.service';



@Injectable()
export class DashProvider {
    constructor(private httpService: HttpService) { }


    init(): any {
        var url = "phleb/dashboard"
        return this.httpService.get(url)
    }

    scan(code): any {
        var url = "phleb/dashboard/scan";
        var requestBody = {
            code: code
        }
        return this.httpService.post(url, requestBody)
    }
    // login(mobile, password): any {
    //     var url = 'v2/sessions';
    //     var requestBody = {
    //         session: {
    //             username: mobile,
    //             password: password
    //         }
    //     }
    //     return this.httpService.post(url, requestBody)
    // }
    // register(terms, group, number, password){
    //     var url = "v2/users/signup"
    //     var requestBody = {
    //         user: {
    //             terms: terms,
    //             group: group,
    //             username: number,
    //             password: password
    //         }
    //     }
    //     return this.httpService.post(url, requestBody)
    // }





}